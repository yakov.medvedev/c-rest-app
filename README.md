# c-rest-app

REST app test task.

Run the program:

- mvn clean package
- java -jar target/restapp-0.0.1-SNAPSHOT.war
- check results in terminal
   - curl -i -X POST -H "Content-Type: application/json" -d '{"id":"1"}' http://localhost:8080/user 
   - use Postman (POST, http://localhost:8080/user, body {"id":1})