package com.medvedev.restapp.controllers;

import com.medvedev.restapp.model.User;
import com.medvedev.restapp.service.UserService;
import com.medvedev.restapp.utill.Util;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/user")
public class RestAppController {

    @Autowired
    private UserService productService;

    @PostMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
        public String getUsers(@RequestBody String id) throws IOException {
        if (id == null) throw new IllegalArgumentException();
        Util.writeToLog("Request", id);
        User user = productService.getUserFioById(new JSONObject(id).optInt("id"));
        String result = user == null ? "NULL" : new JSONObject().put("fio", user.getFio()).toString();
        Util.writeToLog("Response", result);
        return result;
    }
}
