package com.medvedev.restapp.utill;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.medvedev.restapp.service.EncryptionService.decryptString;
import static com.medvedev.restapp.service.EncryptionService.encryptString;

public class Util {
    private static Logger log = LoggerFactory.getLogger(Util.class);

    public static void writeToLog(String title, String string){
        log.info(title + " body ---> " + string);
        String encryptedReq = encryptString(string);
        log.info("=== " + title + " encryption: " + encryptedReq);
        log.info("=== " + title + " decryption: " + decryptString(encryptedReq));
    }
}
