package com.medvedev.restapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.UUID;

@Service
public class EncryptionService{

    private static Logger log = LoggerFactory.getLogger(EncryptionService.class);

    private static String secretKey = UUID.randomUUID().toString().replace("-", "");
    private static String salt = "concordthebest";
    private static Cipher cipher;

    private static Cipher getInstance(){
        if (cipher == null) {
            try {
                cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            } catch (Exception e) {
                log.error(e.toString());
            }
        }
        return cipher;
    }

    public static String encryptString(String arg) {
        try {
            byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt.getBytes(), 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec sKey = new SecretKeySpec(tmp.getEncoded(), "AES");

            Cipher cipher = getInstance();
            cipher.init(Cipher.ENCRYPT_MODE, sKey, ivspec);
            return Base64.getEncoder().encodeToString(cipher.doFinal(arg.getBytes("UTF-8")));
        } catch (Exception e) {
            log.error("Error while encrypting: " + e.toString());
        }
        return null;
    }

    public static String decryptString(String arg) {
        try {
            byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(secretKey.toCharArray(), salt.getBytes(), 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec sKey = new SecretKeySpec(tmp.getEncoded(), "AES");

            Cipher cipher = getInstance();
            cipher.init(Cipher.DECRYPT_MODE, sKey, ivspec);
            return new String(cipher.doFinal(Base64.getDecoder().decode(arg)));
        } catch (Exception e) {
            log.error("Error while decrypting: " + e.toString());
        }
        return null;
    }
}
