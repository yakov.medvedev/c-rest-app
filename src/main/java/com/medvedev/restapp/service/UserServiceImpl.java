package com.medvedev.restapp.service;

import com.medvedev.restapp.model.User;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    private Map<Integer,User> users = new HashMap();

    {
        users.put(1, new User(1,"Bob Marley"));
        users.put(23,new User(23,"John Galt"));
        users.put(99,new User(99,"Harry Potter"));
    }

    @Override
    public User getUserFioById(int id) {
        return users.get(id);
    }
}
