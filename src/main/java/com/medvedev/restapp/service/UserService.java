package com.medvedev.restapp.service;

import com.medvedev.restapp.model.User;

public interface UserService {
    User getUserFioById(int id);
}
